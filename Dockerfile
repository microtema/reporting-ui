FROM node:18-alpine

WORKDIR /app/webserver

COPY build /app/build
COPY webserver /app/webserver

ENV PORT=8080

EXPOSE 8080

CMD ["npm", "start"]
