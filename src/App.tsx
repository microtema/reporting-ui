import React, {Component} from 'react';
import {Navigate, NavLink, Route, Routes} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';
import "bootstrap-icons/font/bootstrap-icons.css";
import './App.css';
import ApiStatus from "./api/ApiStatus";
import DefinitionList from "./component/definition/list/DefinitiionList";
import GatewayComponent from "./component/gateway/GatewayComponent";
import DefinitionDetails from "./component/definition/details/DefinitionDetails";
import ProcessList from "./component/process/list/ProcessList";
import ProcessDetails from "./component/process/details/ProcessDetails";
import ProcessMessageList from "./component/message/list/ProcessMessageList";
import ReportList from "./component/report/list/ReportList";
import ReportDetails from "./component/report/details/ReportDetails";
import LogList from "./component/log/list/LogList";
import LogDetails from "./component/log/details/LogDetails";

class App extends Component<any, any> {

    constructor(props: any) {
        super(props);
        this.state = {data: {}, update: true, listening: false}
    }

    navigationActive(e:any) {
        return {
            color: e.isActive ? "#000000" : "#0d6efd",
            textDecoration: "none"
        };
    }

    render() {
        return <div className="app container">
            <ApiStatus/>
            <br />
            <ul className="nav nav-tabs">
                <li className="nav-item">
                    <NavLink to="/definition" className="nav-link" style={this.navigationActive}>Processes</NavLink>
                </li>
                <li className="nav-item">
                    <NavLink to="/process" className="nav-link" style={this.navigationActive}>Instances</NavLink>
                </li>
                <li className="nav-item" >
                    <NavLink to="/message" className="nav-link" style={this.navigationActive}>Messages</NavLink>
                </li>
                <li className="nav-item">
                    <NavLink to="/report" className="nav-link" style={this.navigationActive}>Reports</NavLink>
                </li>
                <li className="nav-item">
                    <NavLink to="/log" className="nav-link" style={this.navigationActive}>Logs</NavLink>
                </li>
            </ul>
            <Routes>
                <Route path={'/definition'} element={<DefinitionList/>} />
                <Route path="/definition/:definitionKey/start/:component" element={<GatewayComponent/>}/>
                <Route path="/definition/:id" element={<DefinitionDetails/>}/>

                <Route path="/process" element={<ProcessList/>}/>
                <Route path="/process/details/:processBusinessKey/:retryCount" element={<ProcessDetails/>}/>
                <Route path="/process/:selectedStatus/:selectedDefinition" element={<ProcessList/>}/>
                <Route path="/process/:selectedStatus/" element={<ProcessList/>}/>

                <Route index path="/message" element={<ProcessMessageList/>}/>
                <Route index path="/message/:selectedStatus/:selectedDefinition" element={<ProcessMessageList/>}/>

                <Route path="/report" element={<ReportList/>}/>
                <Route path="/report/details/:id" element={<ReportDetails/>}/>

                <Route path={'/log'} element={<LogList/>}/>
                <Route path="/log/details/:id" element={<LogDetails/>}/>
                <Route path="/log/:selectedDefinition/:search" element={<LogList/>}/>
                <Route path="/log/:selectedDefinition" element={<LogList/>}/>

                <Route path={'*'} element={<Navigate to="/definition" replace/>} />
            </Routes>
        </div>
    }
}

export default App;
