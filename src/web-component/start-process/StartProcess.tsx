import React, {Component} from 'react';
import {useNavigate, useParams} from "react-router-dom";

import './StartProcess.scss'
import TemplatePropertyComponent from "../../component/bootstrap/template/TemplatePropertyComponent";
import BpmnDiagram from "../../component/bpmn-diagram/BpmnDiagram";
import * as apiGateway from '../../api/ApiGateway'

class StartProcess extends Component<any, any> {

    params = useParams()
    navigation = useNavigate()
   // location = useLocation()

    constructor(props: any) {
        super(props)
        this.state = {
            template: {properties: []},
            running: false
        };
    }

    componentDidMount() {

        const location = {search:''} // this.location;
        const attributes = new URLSearchParams(location.search);

        const boundedContext = this.params.component

        apiGateway.elementTemplate(boundedContext).subscribe((template: any) => {
            attributes.forEach((value: string, name: string) => template.properties.filter((it: any) => it.name === name).forEach((it: any) => it.value = value))
            this.setState({template})
        })
    }

    render() {
        return <div className="wc-start-process w-100">
            <br/>
            <legend>{this.state.template.label}</legend>
            <div className="alert alert-warning" role="alert">
                {this.state.template.description}
            </div>
            <fieldset className="w-50">
                {
                    this.state.template.properties.map((it: any, index: number) => <TemplatePropertyComponent
                        key={index} data={it} onChange={(e: any) => this.onChangeProperty(e)}/>)
                }
                <button type="button" className="btn btn-primary" onClick={() => this.startProcess()}
                        disabled={this.state.running}>Start
                    Process
                </button>
            </fieldset>
            <br/>
            <BpmnDiagram activities={[]} definitionKey={this.props.match.params.definitionKey}/>
        </div>
    };

    startProcess() {

        this.setState({running: true})

        const fileProperty: any = this.state.template.properties.find((it: any) => it.type === 'File' && it.value)

        const data: any = {}

        this.state.template.properties.forEach(({name, value}: { name: any, value: any }) => (data[name] = value))

        if (fileProperty && fileProperty.value) {

            const formData = fileProperty.value

            formData.append(this.state.template.name, JSON.stringify(data))

            apiGateway.upload(fileProperty.serviceUrl, formData).subscribe((it: any) => this.handleResponse(it))

            this.showProcessView()

        } else {

            apiGateway.post(this.state.template.serviceUrl, data).subscribe((it: any) => this.handleResponse(it))

            this.showProcessView()
        }
    }

    onChangeProperty(event: any) {

        const template = {...this.state.template};

        template.properties.filter((it: any) => it.id === event.id).forEach((it: any) => it.value = event.value)

        this.setState({template})
    }

    showProcessView() {

        const {definitionKey} = this.props.match.params

        this.navigation('/process/ALL/' + definitionKey)
    }

    handleResponse(e: any) {

        console.log('Process status: ', e)
    }
}

export default StartProcess
