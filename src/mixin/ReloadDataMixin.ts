import {Component} from "react";
import {BehaviorSubject} from "rxjs";
import {debounceTime} from "rxjs/operators";

export class ReloadDataMixin extends Component<any, any> {

    timerId: any
    loadDataBehaviorSubject: any;

    componentDidMount() {

        this.loadDataBehaviorSubject = new BehaviorSubject({});

        this.loadDataBehaviorSubject.pipe(
            debounceTime(200))
            .subscribe((it: any) => this.loadData(it))

        this.timerId = setInterval(() => this.loadDataDebounce({}), 10000)

        this.loadData()
    }

    componentWillUnmount() {
        this.loadDataBehaviorSubject.unsubscribe()
        if (this.timerId) {
            clearTimeout(this.timerId);
            this.timerId = undefined;
        }
    }

    loadData(data?: any) {
        console.log('load data', data)
    }

    loadDataDebounce(data?: any) {

        if (data) {
            this.setState({...data}, () => this.loadDataBehaviorSubject.next(data))
        } else {
            this.loadDataBehaviorSubject.next({})
        }
    }

    sortData(propertyName: any) {

        if (this.state.sortBy === propertyName) {
            this.setState({sortBy: '!' + propertyName}, () => this.loadData())
        } else {
            this.setState({sortBy: propertyName}, () => this.loadData())
        }
    }

    getSortClassName(propertyName: string) {

        if (this.state.sortBy === propertyName) {

            return 'bi bi-arrow-up'
        }

        if (this.state.sortBy === '!' + propertyName) {

            return 'bi bi-arrow-down'
        }

        return 'bi bi-arrow-down-up'
    }
}
