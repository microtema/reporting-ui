const { createProxyMiddleware } = require('http-proxy-middleware');

const services = process.env.REST_API_SERVERS.split(',').filter(it => it)

module.exports = function(webServer) {

    console.log('services', services)

    services.forEach(it => {

        const url = new URL(it + '/rest/api')
        const service = url.pathname.split('/')[1]

        const filter = '/api/' + service
        const target = url.protocol + '//' + url.host
        const pathRewrite = {['^' + filter]: url.pathname}

        webServer.use(filter, createProxyMiddleware({changeOrigin: true, target, pathRewrite}))
    })
}