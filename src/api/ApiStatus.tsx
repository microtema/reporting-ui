import React, {useEffect, useState} from 'react';
import subject from "./ApiGateway";

function ApiStatus() {

    const [progress, setProgress] = useState(0);

    useEffect(() => {
        subject.subscribe((it: any) => setProgress(it))
    }, [])

    return (
        <div className="progress" style={{'height': '1px'}}>
            <div className="progress-bar" role="progressbar" style={{'width': progress + '%'}}></div>
        </div>
    );
}

export default ApiStatus
