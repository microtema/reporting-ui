import Axios from 'axios-observable'
import {Observable, of, Subject} from "rxjs";
import {catchError, map} from "rxjs/operators";
import FileSaver from 'file-saver'

const http: Axios = Axios.create({});

const subject = new Subject();

const handleError = (result: any, timerId: any) => {

    return (error: any): Observable<any> => {

        clearTimeout(timerId)
        subject.next(0)

        if (error.response) {
            // The request was made and the server responded with a status code
            // that falls out of the range of 2xx
            console.log('error.response', error.response);
        } else if (error.request) {
            // The request was made but no response was received
            // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
            // http.ClientRequest in node.js
            console.log('error.request', error.request);
        } else {
            // Something happened in setting up the request that triggered an Error
            console.log('Error', error.message);
        }

        console.log('error.config', error.config);

        return of(result);
    }
}

const get = <T>(url: string, fallBackData?: any): Observable<T> => {
    let progress = 0
    const timerId = setInterval(() => {

        if (progress > 75) {
            progress += 5
        } else if (progress > 95) {
            progress -= 25
        } else {
            progress += 25
        }

        subject.next(progress)
    }, 200)
    return http.get<T>(url).pipe(
        map((it: any) => {
            clearTimeout(timerId)
            subject.next(0)
            return it.data
        }),
        catchError(handleError(fallBackData, timerId))
    );
};

const post = <T>(url: string, data: any, fallBackData?: any): Observable<T> => {
    let progress = 0
    const timerId = setInterval(() => {

        if (progress > 75) {
            progress += 5
        } else if (progress > 95) {
            progress -= 25
        } else {
            progress += 25
        }

        subject.next(progress)
    }, 200)
    return http.post<T>(url, data).pipe(
        map((it: any) => {
            clearTimeout(timerId)
            subject.next(0)
            return it.data
        }),
        catchError(handleError(fallBackData, timerId))
    );
};

const upload = <T>(url: string, data: FormData, fallBackData?: any): Observable<T> => {
    let progress = 0
    const timerId = setInterval(() => {

        if (progress > 75) {
            progress += 5
        } else if (progress > 95) {
            progress -= 25
        } else {
            progress += 25
        }

        subject.next(progress)
    }, 200)
    return http.post<T>(url, data, {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    }).pipe(
        map((it: any) => {
            clearTimeout(timerId)
            subject.next(0)
            return it.data
        }),
        catchError(handleError(fallBackData, timerId))
    );
};

const download = <T>(url: string, mediaType: string, fileName: any) => {
    let progress = 0
    const timerId = setInterval(() => {

        if (progress > 75) {
            progress += 5
        } else if (progress > 95) {
            progress -= 25
        } else {
            progress += 25
        }

        subject.next(progress)
    }, 200)
    return http.get<T>(url, {
        headers: {
            Accept: mediaType,
            responseType: 'blob'
        }
    }).pipe(
        map((it: any) => {
            clearTimeout(timerId)
            subject.next(0)
            return it.data
        })
    ).subscribe((it: any) => FileSaver.saveAs(new Blob([it], {type: "text/plain;charset=utf-8"}), fileName));
}

const defaultPageData = {content: [], totalPages: 0}
const defaultListData: any = []

const definitionList = (queryParams?: any) => get('/api/reporting-service/definition?query=' + (queryParams || {search: ''}).search + '&timeRange=' + (queryParams || {selectedTimeRange: 'ALL'}).selectedTimeRange, defaultPageData)
const definitionGet = (id: string, queryParams?: any) => get('/api/reporting-service/definition/' + id + '?timeRange=' + (queryParams || {selectedTimeRange: 'ALL'}).selectedTimeRange, {})
const definitionByKey = (definitionKey: string) => get('/api/reporting-service/definition/key/' + definitionKey, {})
const downloadDefinitionList = (queryParams: any, mediaType: any) => download('/api/reporting-service/definition?query=' + queryParams.search + '&timeRange=' + queryParams.selectedTimeRange, mediaType, 'Definition-List.csv')

const processList = (queryParams: any) => get('/api/reporting-service/process?definitionKey=' + queryParams.selectedDefinition + '&query=' + queryParams.search + '&page=' + (queryParams.page || 0) + '&status=' + (queryParams.selectedStatus || '') + '&properties=' + queryParams.sortBy + '&timeRange=' + queryParams.selectedTimeRange, defaultPageData)
const processGet = (id: string) => get('/api/reporting-service/process/' + id, {})
const processByBusinessKey = (key: string, retryCount: number) => get('/api/reporting-service/process/businessKey/' + key + '/' + retryCount, {})
const processStatus = () => get('/api/reporting-service/process/status', defaultListData)
const downloadProcessList = (queryParams: any, mediaType: any) => download('/api/reporting-service/process?definitionKey=' + queryParams.selectedDefinition + '&query=' + queryParams.search + '&page=' + (queryParams.page || 0) + '&status=' + (queryParams.selectedStatus || '') + '&properties=' + queryParams.sortBy + '&timeRange=' + queryParams.selectedTimeRange, mediaType, 'Process-List.csv')

const messageList = (queryParams: any) => get('/api/reporting-service/message?definitionKey=' + queryParams.selectedDefinition + '&query=' + queryParams.search + '&page=' + (queryParams.page || 0) + '&status=' + (queryParams.selectedStatus || '') + '&properties=' + queryParams.sortBy + '&timeRange=' + queryParams.selectedTimeRange, defaultPageData)
const downloadMessageList = (queryParams: any, mediaType: any) => download('/api/reporting-service/message?definitionKey=' + queryParams.selectedDefinition + '&query=' + queryParams.search + '&page=' + (queryParams.page || 0) + '&status=' + (queryParams.selectedStatus || '') + '&properties=' + queryParams.sortBy + '&timeRange=' + queryParams.selectedTimeRange, mediaType, 'Message-List.csv')

const reportGet = (id: string) => get('/api/reporting-service/report/' + id, {})
const reportStatus = () => get('/api/reporting-service/report/status', defaultListData)
const transactionReports = (transactionId: string, retryCount: number) => get('/api/reporting-service/report/transaction/' + transactionId + '/' + retryCount, defaultListData)
const reportList = (queryParams: any) => get('/api/reporting-service/report?definitionKey=' + queryParams.selectedDefinition + '&query=' + queryParams.search + '&page=' + (queryParams.page || 0) + '&status=' + (queryParams.selectedStatus || '') + '&timeRange=' + queryParams.selectedTimeRange, defaultPageData)
const downloadReportList = (queryParams: any, mediaType: any) => download('/api/reporting-service/report?definitionKey=' + queryParams.selectedDefinition + '&query=' + queryParams.search + '&page=' + (queryParams.page || 0) + '&status=' + (queryParams.selectedStatus || '') + '&timeRange=' + queryParams.selectedTimeRange, mediaType, 'Report-List.csv')

const logGet = (id: string) => get('/api/reporting-service/log/' + id, {})
const logLevel = () => get('/api/reporting-service/log/level', defaultListData)
const runtimeList = () => get('/api/reporting-service/log/runtime', defaultListData)
const logList = (queryParams: any) => get('/api/reporting-service/log?runtimeName=' + queryParams.selectedDefinition + '&query=' + queryParams.search + '&page=' + (queryParams.page || 0) + '&logLevel=' + (queryParams.selectedStatus || '') + '&properties=' + queryParams.sortBy + '&timeRange=' + queryParams.selectedTimeRange, defaultPageData)
const downloadLogList = (queryParams: any, mediaType: any) => download('/api/reporting-service/log?runtimeName=' + queryParams.selectedDefinition + '&query=' + queryParams.search + '&page=' + (queryParams.page || 0) + '&logLevel=' + (queryParams.selectedStatus || '') + '&properties=' + queryParams.sortBy + '&timeRange=' + queryParams.selectedTimeRange, mediaType, 'Log-List.csv')

const timeRangeList = () => get('/api/reporting-service/time-range', defaultListData)
const elementTemplate = (boundedContext: any) => get('/api/' + boundedContext + '/element-template', {properties: []})

const definition = {list: definitionList, get: definitionGet, key: definitionByKey, download: downloadDefinitionList}
const process = {
    list: processList,
    get: processGet,
    businessKey: processByBusinessKey,
    status: processStatus,
    download: downloadProcessList
}
const message = {list: messageList, download: downloadMessageList}
const report = {
    list: reportList,
    transactions: transactionReports,
    get: reportGet,
    status: reportStatus,
    download: downloadReportList
}
const log = {list: logList, get: logGet, runtimes: runtimeList, level: logLevel, download: downloadLogList}
const timeRange = {list: timeRangeList}

export {definition, process, message, report, log, timeRange, elementTemplate, post, upload}

export default subject;

