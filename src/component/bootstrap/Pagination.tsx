import React from 'react';

function Pagination(props: any) {

    return (<nav aria-label="Page navigation">
        <ul className="pagination justify-content-end">
            <li className="page-item">
                                <span className="page-link"
                                      onClick={() => props.updatePage(props.state.page - 1)}>Previous</span>
            </li>
            <li className="page-item disabled">
                                <span
                                    className="page-link"><span>{(props.state.page * props.state.numberOfElements) || 0}-{(props.state.currentElements + (props.state.page * props.state.numberOfElements) || 0)} from {props.state.totalElements}</span></span>
            </li>
            <li className="page-item" onClick={() => props.updatePage(props.state.page + 1)}>
                <span className="page-link">Next</span>
            </li>
        </ul>
    </nav>);
}

export default Pagination
