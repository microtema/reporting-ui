import React, {useState} from 'react';

function SearchFilter(props: any) {

    const [options, showOptions] = useState(false);

    return (<div className="d-flex">
        <input className="form-control me-2" type="search" placeholder="Search" aria-label="Search"
               onChange={(e: any) => props.onChange({search: e.target.value})} value={props.data.search}/>
        <select className="form-select me-2" onChange={(e: any) => props.onChange({selectedDefinition: e.target.value})}
                value={props.data.selectedDefinition}>
            {
                (props.data.availableDefinitionList || []).map((it: any, index: any) => <option key={index}
                                                                                                value={it.definitionKey}>{it.displayName}</option>)
            }
            <option value={''}>All Processes</option>
        </select>
        <select className="form-select me-2" onChange={(e: any) => props.onChange({selectedStatus: e.target.value})}
                value={props.data.selectedStatus}>
            {
                (props.data.availableStatusList || []).map((it: any, index: any) => <option key={index}
                                                                                            value={it.value}>{it.displayName}</option>)
            }
        </select>
        <select className="form-select me-4" onChange={(e: any) => props.onChange({selectedTimeRange: e.target.value})}
                value={props.data.selectedTimeRange}>
            {
                (props.data.availableTimeRangeList || []).map((it: any, index: any) => <option key={index}
                                                                                               value={it.value}>{it.displayName}</option>)
            }
        </select>
        <button className="btn btn-outline-primary me-4" onClick={() => props.onChange({})}>Search</button>
        <div className="dropdown">
            <button className="btn btn-outline-primary dropdown-toggle" type="button" id="dropdownMenuButton1"
                    data-bs-toggle="dropdown" aria-expanded="false" onClick={() => showOptions(!options)}>
                <i className="bi-download"></i>
            </button>
            <ul className={options ? "dropdown-menu show" : "dropdown-menu"} aria-labelledby="dropdownMenuButton1">
                {
                    (props.data.availableOptionList || []).map((it: any, index: any) => <li key={index} onClick={() => {
                        showOptions(false);
                        props.onContextMenuChange(it.value)
                    }}>
                        <div className="dropdown-item">{it.displayName}</div>
                    </li>)
                }
            </ul>
        </div>
    </div>);
}

export default SearchFilter
