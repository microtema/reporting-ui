import React from 'react';

function ProcessState(props: any) {


    const map: any = {
        STARTED: 'bi-arrow-clockwise text-primary',
        INFO: 'bi-info-circle text-success',
        DEBUG: 'bi-info-circle text-primary',
        PROCESS_RUNNING: 'bi-arrow-clockwise text-primary',
        COMPLETED: 'bi-check-circle text-success',
        PROCESS_COMPLETED: 'bi-check-circle text-success',
        WARNING: 'bi-exclamation-triangle text-warning',
        WARN: 'bi-exclamation-triangle text-warning',
        ERROR: 'bi-exclamation-triangle text-danger',
        ALL: 'bi-circle text-primary'
    }

    let className = 'bi ' + map[props.data];

    return (
        <div className="process-state">
            <span title={props.title || props.data}><i className={className}></i></span> {props.label || ''}
        </div>
    );
}

export default ProcessState;
