import React from 'react';
import moment from 'moment';

function DateTime(props: any) {

    const format = props.format || 'DD/MM/YYYY hh:mm:ss';

    return (
        <div className="process-state">
            <span>{props.data ? moment(props.data).format(format) : ''}</span>
        </div>
    );
}

export default DateTime;
