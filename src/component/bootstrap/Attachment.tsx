import React, {useState} from 'react';
import Message from "./Message";

function Attachment(props: any) {

    const [count, setCount] = useState(0);

    if (props.data) {
        return (
            <div className="process-state" title={props.data} onClick={() => {
                setCount(count + 1)
                navigator.clipboard.writeText(props.data)
            }}>
                <span>{props.data ? <i className={'bi bi-braces' + (count ? '-fill' : '')}></i> : ''}</span>
            </div>
        );
    } else if (props.message) {
        return <Message data={props.message}/>
    } else {
        return null;
    }
}

export default Attachment;
