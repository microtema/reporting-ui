import React from 'react';
import prettyMilliseconds from 'pretty-ms'

function Duration(props: any) {

    return (
        <div className="bs-duration">
            <span>{prettyMilliseconds(props.data || 0)}</span>
        </div>
    );
}

export default Duration;
