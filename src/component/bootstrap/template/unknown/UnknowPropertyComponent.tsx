import React from 'react';

function UnknownPropertyComponent(props: any) {

    return (
        <div className="unknown-property-component">
            <span className="alert alert-warning">Unknown {props.data.label}</span>
        </div>
    );
}

export default UnknownPropertyComponent;
