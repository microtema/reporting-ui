import React from 'react';

function DropdownPropertyComponent(props: any) {

    const {onChange} = props;
    const {id, name, label, value, choices, description} = props.data;

    return (
        <div className="dropdown-property-component">
            <div className="mb-3">
                <label htmlFor={id} className="form-label">{label}</label>
                <select name={name} id={id} className="form-select" value={value}
                        onChange={(e: any) => onChange({id, value: e.target.value})}>
                    {
                        choices.map((it: any, index: number) => <option key={index} value={it.value}>{it.name}</option>)
                    }
                </select>
                <div id={id + 'Description'} className="form-text">{description}</div>
            </div>
        </div>
    );
}

export default DropdownPropertyComponent;
