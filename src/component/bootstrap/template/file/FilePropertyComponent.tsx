import React, {useState} from 'react';
import {FileUploader} from "react-drag-drop-files";
import './FilePropertyComponent.scss'

function FilePropertyComponent(props: any) {

    const [file, setFile] = useState(null);

    const {onChange} = props;
    const {id, name, label, choices, description} = props.data;
    const types: any = choices.map((it: any) => it.value)

    return (
        <div className="file-property-component">
            <div className="mb-3">
                <label htmlFor={id} className="form-label">{label}</label>
                <div className="file-upload">
                    <FileUploader aria-describedby={id + 'Description'}
                                  handleChange={(file: any) => {
                                      setFile(file)
                                      const formData = new FormData() as any
                                      formData.append(name, file)
                                      onChange({id, value: formData})
                                  }}
                                  name={name}
                                  types={types}/>
                </div>
                <div id={id + 'Description'}
                     className="form-text">{description} / {file ? `File name: ${(file as any).name}` : "No files uploaded yet"}</div>
            </div>
        </div>
    );
}

export default FilePropertyComponent;
