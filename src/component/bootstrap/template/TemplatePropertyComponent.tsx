import React from 'react';
import InputPropertyComponent from "./input/InputPropertyComponent";
import CheckboxPropertyComponent from "./checkbox/CheckboxPropertyComponent";
import UnknownPropertyComponent from "./unknown/UnknowPropertyComponent";
import DropdownPropertyComponent from "./dropdown/DropdownPropertyComponent";
import FilePropertyComponent from "./file/FilePropertyComponent";

const components: any = {
    String: InputPropertyComponent,
    Dropdown: DropdownPropertyComponent,
    Boolean: CheckboxPropertyComponent,
    File: FilePropertyComponent
}

function TemplatePropertyComponent(props: any) {

    const component = components[props.data.type] || UnknownPropertyComponent

    return React.createElement(component, props, null);
}

export default TemplatePropertyComponent;
