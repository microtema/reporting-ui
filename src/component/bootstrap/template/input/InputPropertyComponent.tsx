import React from 'react';

function InputPropertyComponent(props: any) {

    const {onChange} = props;
    const {id, name, label, value, description} = props.data;

    return (
        <div className="input-property-component">
            <div className="mb-3">
                <label htmlFor={id} className="form-label">{label}</label>
                <input type="text" className="form-control" name={name} id={id} aria-describedby={id + 'Description'}
                       value={value} onChange={(e: any) => onChange({id, value: e.target.value})}/>
                <div id={id + 'Description'} className="form-text">{description}</div>
            </div>
        </div>
    );
}

export default InputPropertyComponent;
