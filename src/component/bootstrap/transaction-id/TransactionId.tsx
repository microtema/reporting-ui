import React from 'react';
import './TransactionId.scss'

function TransactionId(props: any) {

    const shortId = () => {
        return props.data.substring(0, 8)
    }

    return (
        <div className="transaction-id" title={props.data}
             onClick={() => navigator.clipboard.writeText(props.data)}>
            {shortId()} <i className="bi bi-back"></i>
        </div>
    );
}

export default TransactionId;
