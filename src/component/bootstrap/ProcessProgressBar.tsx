import React from 'react';

const getPercentage = (count: number, total: number) => total ? Number(100 / total * count).toFixed(2) : 0

function ProcessProgressBar(props: any) {

    const {entries, total} = (props.data || {entries: [], total: 0})

    return (
        <div className="progress" style={{height: '8px'}}>
            {
                entries.map((it: any, index: number) => {
                    const percentage = getPercentage(it.count, total)
                    return (<div className="progress-bar" key={index}
                                 style={{
                                     width: percentage + '%',
                                     backgroundColor: it.color
                                 }}
                                 title={it.name + ' ' + percentage + '%'}>&nbsp;</div>)
                })
            }
        </div>
    );
}

export default ProcessProgressBar;
