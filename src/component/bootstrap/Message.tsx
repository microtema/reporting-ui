import React, {useState} from 'react';

function Message(props: any) {

    const [count, setCount] = useState(0);

    return (
        <div className="b-message" title={props.data} onClick={() => {
            setCount(count + 1)
            navigator.clipboard.writeText(props.data)
        }}>
            {props.data ? <i className={'text-danger bi bi-lightning' + (count ? '-fill' : '')}></i> : ''}
        </div>
    );
}

export default Message;
