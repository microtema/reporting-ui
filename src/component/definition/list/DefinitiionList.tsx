import React from 'react';
import * as apiGateway from '../../../api/ApiGateway'
import './DefinitionList.scss'
import DateTime from "../../bootstrap/DateTime";
import {ReloadDataMixin} from "../../../mixin/ReloadDataMixin";
import moment from "moment";
import ProcessProgressBar from "../../bootstrap/ProcessProgressBar";
import withRouter from "../../../mixin/withRouter";

class DefinitionList extends ReloadDataMixin {

    processColors: Array<any> = ['#2b7489', '#e34c26', '#f1e05a', '#0db7ed', '#28a745', '#2639e3', '#26e3c7', '#e16546', '#e1cc46', '#8c46e1', '#d746e1', '#e1ab46', '#1c5644', '#703324', '#223259'];

    constructor(props: any) {
        super(props)
        this.state = {
            entries: [],
            search: '',
            availableOptionList: [{value: "download-as-csv", displayName: "Download as CSV File"}],
            selectedTimeRange: 'ALL',
            availableTimeRangeList: [],
            showOptions: false
        }
    }

    componentDidMount() {

        super.componentDidMount()

        apiGateway.timeRange.list()
            .subscribe((availableTimeRangeList: any) => this.setState({availableTimeRangeList}))
    }

    render() {

        return <div className="definition-list">
            <br/>
            <div className="row mb-3">
                <div className="col-8">
                    <form className="d-flex">
                        <input className="form-control me-2" type="search" placeholder="Search" aria-label="Search"
                               onChange={(e: any) => this.loadDataDebounce({search: e.target.value})}/>
                        <select className="form-select me-4"
                                onChange={(e: any) => this.loadDataDebounce({selectedTimeRange: e.target.value})}
                                value={this.state.selectedTimeRange}>
                            {
                                (this.state.availableTimeRangeList || []).map((it: any, index: any) => <option
                                    key={index}
                                    value={it.value}>{it.displayName}</option>)
                            }
                        </select>
                        <button className="btn btn-outline-primary me-4" type="submit"
                                onClick={() => this.loadDataDebounce({})}>Search
                        </button>
                        <div className="dropdown">
                            <button className="btn btn-outline-primary dropdown-toggle" type="button"
                                    id="dropdownMenuButton1"
                                    data-bs-toggle="dropdown" aria-expanded="false"
                                    onClick={() => this.showOptions(!this.state.showOptions)}>
                                <i className="bi-download"></i>
                            </button>
                            <ul className={this.state.showOptions ? "dropdown-menu show" : "dropdown-menu"}
                                aria-labelledby="dropdownMenuButton1">
                                {
                                    (this.state.availableOptionList || []).map((it: any, index: any) => <li key={index}
                                                                                                            onClick={() => {
                                                                                                                this.showOptions(false);
                                                                                                                this.onContextMenuChange(it.value)
                                                                                                            }}>
                                        <div className="dropdown-item">{it.displayName}</div>
                                    </li>)
                                }
                            </ul>
                        </div>
                    </form>
                </div>
            </div>
            <br/>
            <div className="row">
                <div className="col">
                    <div className="card shadow rounded border border-primary"
                         onDoubleClick={() => this.showProcesses('STARTED')}>
                        <div className="card-body">
                            <h5 className="card-title text-center">In Progress</h5>
                            <br/>
                            <h2 className="card-subtitle mb-2 text-muted text-center"><span
                                className="text-primary"> {this.progressCount()}</span>
                            </h2>
                        </div>
                        <div className="card-footer"><ProcessProgressBar
                            data={this.getProcessProgressData('progressCount')}/></div>
                    </div>
                </div>
                <div className="col">
                    <div className="card shadow rounded border border-success"
                         onDoubleClick={() => this.showProcesses('COMPLETED')}>
                        <div className="card-body">
                            <h5 className="card-title text-center">Completed</h5>
                            <br/>
                            <h2 className="card-subtitle mb-2 text-muted text-center"><span
                                className="text-success">{this.completedCount()}</span>
                            </h2>
                        </div>
                        <div className="card-footer"><ProcessProgressBar
                            data={this.getProcessProgressData('completedCount')}/></div>
                    </div>
                </div>
                <div className="col">
                    <div className="card shadow rounded border border-warning"
                         onDoubleClick={() => this.showProcesses('WARNING')}>
                        <div className="card-body">
                            <h5 className="card-title text-center">Warning</h5>
                            <br/>
                            <h2 className="card-subtitle mb-2 text-muted text-center"><span
                                className="text-warning">{this.warningCount()}</span>
                            </h2>
                        </div>
                        <div className="card-footer"><ProcessProgressBar
                            data={this.getProcessProgressData('warningCount')}/></div>
                    </div>
                </div>
                <div className="col">
                    <div className="card shadow rounded border border-danger"
                         onDoubleClick={() => this.showProcesses('ERROR')}>
                        <div className="card-body">
                            <h5 className="card-title text-center">Failed</h5>
                            <br/>
                            <h2 className="card-subtitle mb-2 text-muted text-center"><span
                                className="text-danger">{this.failedCount()}</span>
                            </h2>
                        </div>
                        <div className="card-footer"><ProcessProgressBar
                            data={this.getProcessProgressData('failedCount')}/></div>
                    </div>
                </div>
            </div>
            <br/>
            <hr/>
            <table className="table table-hover table-striped">
                <thead>
                <tr>
                    <th scope="col" className="process-color">&nbsp;</th>
                    <th scope="col" className="display-name">Name</th>
                    <th scope="col">Description</th>
                    <th scope="col" className="version">Version</th>
                    <th scope="col" className="date-time">Created</th>
                    <th scope="col" className="date-time">Last Heart Beat</th>
                    <th scope="col" className="status">&nbsp;</th>
                    <th scope="col" className="status">&nbsp;</th>
                    <th scope="col" className="status">&nbsp;</th>
                    <th scope="col" className="status">&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                {
                    (this.state.entries || []).map((it: any, index: number) => {
                        return <tr key={index} onDoubleClick={this.showDetails.bind(this, it.id)}>
                            <td className={'process-color'}
                                style={{backgroundColor: this.getProcessColor(index)}}>&nbsp;</td>
                            <td>{it.displayName}</td>
                            <td title={it.description} className={'text-truncate'}>
                                <span className={'text-muted'}>{it.description}</span>
                            </td>
                            <td>{it.majorVersion + '.' + it.definitionVersion}</td>
                            <td><DateTime data={it.deployTime} format={'DD/MM/YYYY hh:mm'}/></td>
                            <td>
                                <DateTime data={it.lastHeartBeat} format={'DD/MM/YYYY hh:mm'}/>
                                {
                                    this.getDuration(it.lastHeartBeat)
                                }
                            </td>
                            <td className="status"><span
                                className="badge rounded-pill bg-primary">{it.definitionStatus.progressCount}</span>
                            </td>
                            <td className="status"><span
                                className="badge rounded-pill bg-success">{it.definitionStatus.completedCount}</span>
                            </td>
                            <td className="status"><span
                                className="badge rounded-pill bg-warning">{it.definitionStatus.warningCount}</span>
                            </td>
                            <td className="status"><span
                                className="badge rounded-pill bg-danger">{it.definitionStatus.failedCount}</span>
                            </td>
                        </tr>
                    })
                }
                </tbody>
            </table>
        </div>;
    }

    showDetails(id: string) {

        this.props.navigate('/definition/' + id)
    }

    loadData() {

        apiGateway.definition.list(this.state).subscribe((it: any) => this.setState({entries: it.content}))
    }

    getDuration(lastHeartBeat: any) {

        if (!lastHeartBeat) {
            return '---'
        }

        return moment(lastHeartBeat).utc(true).fromNow()
    }

    getProcessColor(index: number) {

        return this.processColors[index]
    }

    getProcessProgressData(type: string) {

        let total = 0;
        let index = 0;

        const entries = (this.state.entries || []).map((it: any) => {
            const count = it.definitionStatus[type]
            total += count
            return {count, name: it.displayName, color: this.getProcessColor(index++)};
        });

        return {entries, total}
    }

    onContextMenuChange(event: any) {

        if (event === 'download-as-csv') {
            apiGateway.definition.download(this.state, 'text/csv');
        }
    }

    showOptions(showOptions: boolean) {
        this.setState({showOptions})
    }

    private progressCount() {

        let count = 0;

        (this.state.entries || []).forEach((it: any) => count += it.definitionStatus.progressCount)

        return count
    }

    private completedCount() {

        let count = 0;

        (this.state.entries || []).forEach((it: any) => count += it.definitionStatus.completedCount)

        return count
    }

    private warningCount() {

        let count = 0;

        (this.state.entries || []).forEach((it: any) => count += (it.definitionStatus.warningCount || 0))

        return count
    }

    private failedCount() {

        let count = 0;

        (this.state.entries || []).forEach((it: any) => count += it.definitionStatus.failedCount)

        return count
    }

    private showProcesses(processStatus: string) {

        this.props.navigate('/process/' + processStatus)
    }
}

export default withRouter(DefinitionList)
