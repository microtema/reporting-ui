import React from 'react';
import BpmnDiagram from "../../bpmn-diagram/BpmnDiagram";
import * as apiGateway from "../../../api/ApiGateway";
import DateTime from "../../bootstrap/DateTime";
import {ReloadDataMixin} from "../../../mixin/ReloadDataMixin";
import moment from "moment";
import ProcessProgressBar from "../../bootstrap/ProcessProgressBar";

import './DefinitionDetails.scss'
import withRouter from "../../../mixin/withRouter";

class DefinitionDetails extends ReloadDataMixin {

    progressDataList = [
        {id: 'progressCount', name: 'In Progress', color: '#0d6efd'},
        {id: 'completedCount', name: 'Completed', color: '#28a745'},
        {id: 'warningCount', name: 'Warning', color: '#ffc107'},
        {id: 'failedCount', name: 'Failed', color: '#dc3545'}]

    constructor(props: any) {
        super(props)
        this.state = {
            definitionStatus: {},
            startProcess: false,
            selectedTimeRange: 'ALL',
            availableTimeRangeList: [],
        }
    }

    componentDidMount() {

        super.componentDidMount()

        apiGateway.timeRange.list()
            .subscribe((availableTimeRangeList: any) => this.setState({availableTimeRangeList}))
    }

    render() {
        return <div className="definition-details">
            <br/>
            <div className="row">
                <div className="col-4">
                    <div className="card-subtitle mb-2 text-muted">Name:</div>
                    <div>{this.state.displayName}</div>
                </div>
                <div className="col-4">
                    <div className="card-subtitle mb-2 text-muted">Version:</div>
                    <div>{this.state.majorVersion + '.' + this.state.definitionVersion}</div>
                </div>
                <div className="col-4">
                    <div className="card-subtitle mb-2 text-muted">Created:</div>
                    <div><DateTime data={this.state.deployTime}/></div>
                </div>
            </div>
            <br/>
            <div className="row">
                <div className="col-4">
                </div>
                <div className="col-4">
                </div>
                <div className="col-4">
                    <div className="card-subtitle mb-2 text-muted">Last Heart Beat:</div>
                    <div>
                        <DateTime data={this.state.lastHeartBeat} format={'DD/MM/YYYY hh:mm'}/>
                        {
                            this.getDuration(this.state.lastHeartBeat)
                        }
                    </div>
                </div>
            </div>
            <br/>
            <div className="card-subtitle mb-2 text-muted">Description:</div>
            <p>{this.state.description}</p>
            <hr/>
            <div className="row mb-3">
                <div className="col-3">
                    <select className="form-select me-2"
                            onChange={(e: any) => this.loadDataDebounce({selectedTimeRange: e.target.value})}
                            value={this.state.selectedTimeRange}>
                        {
                            (this.state.availableTimeRangeList || []).map((it: any, index: any) => <option
                                key={index}
                                value={it.value}>{it.displayName}</option>)
                        }
                    </select>
                </div>
                <div className="col-9">
                    <div className="pagination justify-content-end">
                        <button style={{whiteSpace: 'nowrap'}} type="button"
                                className={this.state.startProcess ? "btn btn-outline-primary" : "hide"}
                                onClick={() => this.startProcess()}>Start Process
                        </button>
                    </div>
                </div>
            </div>
            <br/>
            <div className="row">
                <div className="col">
                    <div className="card shadow rounded border border-primary"
                         onDoubleClick={() => this.showProcesses('STARTED')}>
                        <div className="card-body">
                            <h5 className="card-title text-center">In Progress</h5>
                            <br/>
                            <h2 className="card-subtitle mb-2 text-muted text-center"><span
                                className="text-primary">{this.state.definitionStatus?.progressCount || 0}</span>
                            </h2>
                        </div>
                    </div>
                </div>
                <div className="col">
                    <div className="card shadow rounded border border-success"
                         onDoubleClick={() => this.showProcesses('COMPLETED')}>
                        <div className="card-body">
                            <h5 className="card-title text-center">Completed</h5>
                            <br/>
                            <h2 className="card-subtitle mb-2 text-muted text-center"><span
                                className="text-success">{this.state.definitionStatus?.completedCount || 0}</span>
                            </h2>
                        </div>
                    </div>
                </div>
                <div className="col">
                    <div className="card shadow rounded border border-warning"
                         onDoubleClick={() => this.showProcesses('WARNING')}>
                        <div className="card-body">
                            <h5 className="card-title text-center">Warning</h5>
                            <br/>
                            <h2 className="card-subtitle mb-2 text-muted text-center"><span
                                className="text-warning">{this.state.definitionStatus?.warningCount || 0}</span>
                            </h2>
                        </div>
                    </div>
                </div>
                <div className="col">
                    <div className="card shadow rounded border border-danger"
                         onDoubleClick={() => this.showProcesses('ERROR')}>
                        <div className="card-body">
                            <h5 className="card-title text-center">Failed</h5>
                            <br/>
                            <h2 className="card-subtitle mb-2 text-muted text-center"><span
                                className="text-danger">{this.state.definitionStatus?.failedCount || 0}</span>
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <ProcessProgressBar data={this.getProgressData()}/>
            <br/>
            <BpmnDiagram diagram={this.state.diagram}/>
        </div>
    };

    showProcesses(instanceStatus: string) {

        this.props.navigate('/process/' + instanceStatus + '/' + this.state.definitionKey)
    }

    loadData() {

       // const id = this.props.match.params.id;
        const id = this.props.params.id as string;

        apiGateway.definition.get(id, this.state).subscribe((data: any) => {
            this.setState({...data})
            apiGateway.elementTemplate(data.boundedContext).subscribe((template: any) => this.setState({startProcess: template.properties.length > 0}))
        })
    }

    getDuration(lastHeartBeat: any) {

        if (!lastHeartBeat) {
            return '---'
        }

        return moment(lastHeartBeat).utc(true).fromNow()
    }

    getProgressData() {

        const definitionStatus = this.state.definitionStatus

        if (!definitionStatus) {
            return {total: 0, entries: []}
        }

        let total = 0;

        const entries = this.progressDataList.map((it: any) => {
            const count = definitionStatus[it.id]
            total += count
            return {...it, count}
        })

        return {total, entries}
    }

    startProcess() {
        this.props.navigate('/definition/' + this.state.definitionKey + '/start/' + this.state.boundedContext)
    }
}

export default withRouter(DefinitionDetails)
