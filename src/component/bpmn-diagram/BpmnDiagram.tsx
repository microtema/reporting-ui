import React, {Component} from 'react';
import BpmnJS from 'bpmn-js/dist/bpmn-navigated-viewer.production.min.js';
import * as apiGateway from "../../api/ApiGateway";
import './BpmnDiagram.scss'

class BpmnDiagram extends Component<any, any> {

    private viewer: any;
    private canvas: any;
    private eventBus: any;
    private elementRegistry: any;
    private missingDiagram = false;

    componentDidMount() {

        this.viewer = new BpmnJS({
            container: '.diagram-container'
        })
    }

    componentDidUpdate(prevProps: Readonly<any>, prevState: Readonly<any>, snapshot?: any) {

        if (this.missingDiagram) {
            return;
        }

        if (this.canvas) {
            this.elementRegistry = this.viewer.get('elementRegistry') || 'foo'
            this.addMarkers();
            return;
        }

        if (this.props.diagram) {
            this.importXML(this.props.diagram)
        } else if (this.props.definitionKey) {
            apiGateway.definition.key(this.props.definitionKey).subscribe((it: any) => this.importXML(it?.diagram))
        }
    }

    render() {

        if (this.missingDiagram) {
            return <section>
                <div className="alert alert-danger" role="alert">
                    <i className='bi-exclamation-triangle text-danger'>&nbsp;</i> Missing Diagram!
                </div>
            </section>
        }

        return <div className="bpmn-diagram">
            <div className="diagram-container"></div>
        </div>
    }

    importXML(diagram: string) {

        if (!diagram) {

            this.missingDiagram = true;
            return;
        }

        this.viewer.importXML(diagram).then(() => {
            this.canvas = this.viewer.get('canvas')
            this.eventBus = this.viewer.get('eventBus')
            this.elementRegistry = this.viewer.get('elementRegistry')
            this.eventBus.on('selection.changed', this.onSelectionChanged.bind(this))
            this.canvas.zoom('fit-viewport', true)
            this.addMarkers()
        })
    }

    onSelectionChanged(e: any) {

    }

    addMarkers() {

        (this.props.activities || []).forEach((it: any) => {
            try {
                this.canvas.addMarker(it.activityId, it.processStatus)
            } catch (e) {
                console.log("Unable to find element by ", it.activityId)
            }
        })
    }
}

export default BpmnDiagram
