import React from 'react';
import * as apiGateway from "../../../api/ApiGateway";
import ProcessState from "../../bootstrap/status/ProcessState";
import DateTime from "../../bootstrap/DateTime";
import './LogDetails.scss'
import {ReloadDataMixin} from "../../../mixin/ReloadDataMixin";
import withRouter from "../../../mixin/withRouter";

class LogDetails extends ReloadDataMixin {

    constructor(props: any) {
        super(props)
        this.state = {
            data: {}
        }
    }

    render() {
        return <div className="log-details">
            <br/>
            <div className="row">
                <div className="col-4">
                    <div className="card-subtitle mb-2 text-muted">Process Name:</div>
                    <div onClick={() => this.showLogs(this.state.data.runtimeName, '')}
                         className="text-decoration-none btn-link">{this.state.data.runtimeName}</div>
                </div>
                <div className="col-4">
                    <div className="card-subtitle mb-2 text-muted">Process ID:</div>
                    <div onClick={() => this.showLogs(this.state.data.runtimeName, this.state.data.runtimeId)}
                         className="text-decoration-none btn-link">{this.state.data.runtimeId}</div>
                </div>
                <div className="col-4">
                    <div className="card-subtitle mb-2 text-muted">Timestamp:</div>
                    <div className="text-decoration-none">
                        <div><DateTime data={this.state.data.timestamp}/></div>
                    </div>
                </div>
            </div>
            <br/>
            <div className="row">
                <div className="col-4">
                    <div className="card-subtitle mb-2 text-muted">Level:</div>
                    <div><ProcessState data={this.state.data.level}/></div>
                </div>
            </div>
            <hr/>
            <br/>
            <div className="row g-2">
                <div className="col-md">
                    <h5>Message</h5>
                </div>
                <div className="col-md">
                    <h6 className="float-end text-decoration-none btn-link"
                        onClick={() => navigator.clipboard.writeText(this.state.data.message)}>&nbsp;Copy</h6>
                </div>
                <div className="shadow p-3 mb-5 bg-body rounded">
                    <code dangerouslySetInnerHTML={{__html: this.state.data.message}}/>
                </div>
            </div>
            <div className="row g-2">
                <div className="col-md">
                    <h5>Stack Trace</h5>
                </div>
                <div className="col-md">
                    <h6 className="float-end text-decoration-none btn-link"
                        onClick={() => navigator.clipboard.writeText(this.state.data.stackTrace)}>&nbsp;Copy</h6>
                </div>
                <div className="shadow p-3 mb-5 bg-body rounded">
                    <code dangerouslySetInnerHTML={{__html: this.state.data.stackTrace || '&nbsp;'}}/>
                </div>
            </div>
        </div>
    };

    loadData(data: any) {

        let id = this.props.params.id as string
        if (data && data.id) {
            id = data.id
        }

        apiGateway.log.get(id).subscribe((data: any) => this.setState({data}))
    }

    showLogs(runtimeName: string, runtimeId: string) {

        this.componentWillUnmount();

        this.props.navigate('/log/' + runtimeName + '/' + runtimeId)
    }
}

export default withRouter(LogDetails)
