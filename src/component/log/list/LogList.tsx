import React from 'react';

import {ReloadDataMixin} from "../../../mixin/ReloadDataMixin";
import SearchFilter from "../../bootstrap/SearchFilter";
import './LogList.scss'
import Pagination from "../../bootstrap/Pagination";
import * as apiGateway from "../../../api/ApiGateway";
import DateTime from "../../bootstrap/DateTime";
import ProcessState from "../../bootstrap/status/ProcessState";
import withRouter from "../../../mixin/withRouter";

class LogList extends ReloadDataMixin {

    constructor(props: any) {
        super(props)
        this.state = {
            entries: [],
            sortBy: 'logTimestamp',
            totalPages: 0,
            numberOfElements: 25,
            currentElements: 0,
            totalElements: 0,
            page: 0,
            search: '',
            availableOptionList: [{value: "download-as-csv", displayName: "Download as CSV File"}],
            availableDefinitionList: [],
            availableStatusList: [],
            availableTimeRangeList: [],
            selectedDefinition: '',
            selectedStatus: 'ALL',
            selectedTimeRange: 'ALL',
            ...this.props.params
        }
    }

    componentDidMount() {

        super.componentDidMount()

        apiGateway.log.level()
            .subscribe((availableStatusList: any) => this.setState({availableStatusList}))

        apiGateway.timeRange.list()
            .subscribe((availableTimeRangeList: any) => this.setState({availableTimeRangeList}))

        apiGateway.log.runtimes()
            .subscribe((availableDefinitionList: any) => this.setState({availableDefinitionList}, () => this.loadData()))
    }

    render() {
        return <div className="log-list">
            <br/>
            <div className="row mb-3">
                <div className="col-8">
                    <SearchFilter data={this.state}
                                  onChange={(state: any) => this.loadDataDebounce(state)}
                                  onContextMenuChange={(event: any) => this.onContextMenuChange(event)}/>
                </div>
                <div className="col-4">
                    <Pagination updatePage={(e: any) => this.updatePage(e)} state={this.state}/>
                </div>
            </div>
            <table className="table table-hover table-striped">
                <thead>
                <tr>
                    <th scope="col" className="display-name" onClick={() => this.sortData('runtimeName')}>
                        Process Name<i className={this.getSortClassName('runtimeName')}></i>
                    </th>
                    <th scope="col" className="display-name" onClick={() => this.sortData('runtimeId')}>
                        Process ID <i className={this.getSortClassName('runtimeId')}></i>
                    </th>
                    <th scope="col" onClick={() => this.sortData('logMessage')}>
                        Message <i className={this.getSortClassName('logMessage')}></i>
                    </th>
                    <th scope="col" className="date-time" onClick={() => this.sortData('logTimestamp')}>
                        Time <i className={this.getSortClassName('logTimestamp')}></i>
                    </th>
                    <th scope="col" className="stack-trace">
                        S-Trace
                    </th>
                    <th scope="col" className="status" onClick={() => this.sortData('logLevel')}>
                        Level <i className={this.getSortClassName('logLevel')}></i>
                    </th>
                </tr>
                </thead>
                <tbody>
                {
                    this.state.entries.map((it: any, index: number) => {
                        return <tr key={index} onDoubleClick={this.showLog.bind(this, it)}>
                            <td>{it.runtimeName}</td>
                            <td>{it.runtimeId}</td>
                            <td className={'text-truncate'} title={it.message}>{it.message}</td>
                            <td><DateTime data={it.timestamp}/></td>
                            <td align={'center'} title={it.stackTrace}>
                                <i className={it.stackTrace ? 'bi bi-card-text text-danger' : ''}></i>
                            </td>
                            <td align={'center'}><ProcessState data={it.level}/></td>
                        </tr>
                    })
                }
                </tbody>
            </table>
        </div>
    };

    loadData() {

        apiGateway.log.list(this.state)
            .subscribe((it: any) => this.setState({
                entries: it.content,
                totalPages: it.totalPages,
                totalElements: it.totalElements,
                numberOfElements: it.numberOfElements,
                currentElements: it.content.length
            }))
    }

    updatePage(page: number) {

        const minPage = 0
        const maxPage = this.state.totalPages

        if (page < minPage || page >= maxPage || page === this.state.page) {
            return
        }

        this.loadDataDebounce({page})
    }

    showLog(entry: any) {
        this.props.navigate('/log/details/' + entry.id)
    }

    onContextMenuChange(event: any) {

        if (event === 'download-as-csv') {
            apiGateway.log.download(this.state, 'text/csv')
        }
    }
}

export default withRouter(LogList)
