import React from 'react';
import BpmnDiagram from "../../bpmn-diagram/BpmnDiagram";
import Activities from "./Activities";
import * as apiGateway from "../../../api/ApiGateway";
import ProcessState from "../../bootstrap/status/ProcessState";
import DateTime from "../../bootstrap/DateTime";
import Duration from "../../bootstrap/Duration";
import {ReloadDataMixin} from "../../../mixin/ReloadDataMixin";
import withRouter from "../../../mixin/withRouter";

class ProcessDetails extends ReloadDataMixin {

    constructor(props: any) {
        super(props)
        this.state = {data: {}, activities: [], missingInstance: false}
    }

    render() {
        return <div className="report">
            <div style={{display: this.state.missingInstance ? 'block' : 'none'}}>
                <br/>
                <div className={'alert alert-danger'}><i
                    className='bi-exclamation-triangle text-danger'>&nbsp;</i> Unable to find Process Instance for this
                    transaction!
                </div>
            </div>
            <br/>
            <div className="row">
                <div className="col-4">
                    <div className="card-subtitle mb-2 text-muted">Name:</div>
                    <div className="btn-link text-decoration-none"
                         onClick={() => this.showProcesses()}>{this.state.data.instanceName}</div>
                </div>
                <div className="col-4">
                    <div className="card-subtitle mb-2 text-muted">Start Time:</div>
                    <div><DateTime data={this.state.data.instanceStartTime}/></div>
                </div>
                <div className="col-4">
                    <div className="card-subtitle mb-2 text-muted">Started By:</div>
                    <div>{this.state.data.starterId}</div>
                </div>
            </div>
            <br/>
            <div className="row">
                <div className="col-4">
                    <div className="card-subtitle mb-2 text-muted">Transaction ID / Retry Count:</div>
                    <div>{this.state.data.processBusinessKey}{this.state.data.retryCount > 0 ? ' / ' + this.state.data.retryCount : ''}</div>
                </div>
                <div className="col-4">
                    <div className="card-subtitle mb-2 text-muted">End Time:</div>
                    <div><DateTime data={this.state.data.instanceEndTime}/></div>
                </div>
                <div className="col-4">
                    <div className="card-subtitle mb-2 text-muted">Reference Type:</div>
                    <div>{this.state.data.referenceType}</div>
                </div>
            </div>
            <br/>
            <div className="row">
                <div className="col-4">
                    <div className="card-subtitle mb-2 text-muted">Status:</div>
                    <div className={'row'}>
                        <ProcessState data={this.state.data.instanceStatus}
                                      label={this.state.data.instanceDescription}/>
                    </div>
                </div>
                <div className="col-4">
                    <div className="card-subtitle mb-2 text-muted">Duration:</div>
                    <div><Duration data={this.state.data.duration}/></div>
                </div>
                <div className="col-4">
                    <div className="card-subtitle mb-2 text-muted">Reference ID:</div>
                    <div>{this.state.data.referenceId}</div>
                </div>
            </div>
            <hr/>
            <section>
                <BpmnDiagram activities={this.state.activities} definitionKey={this.state.data.definitionKey}/>
            </section>
            <br/>
            <section>
                <ul className="nav nav-tabs">
                    <li className="nav-item">
                        <span className="nav-link active">Activities <span
                            className="badge rounded-pill bg-secondary">{this.state.activities.length}</span></span>
                    </li>
                </ul>
                <Activities entries={this.state.activities}/>
            </section>
        </div>
    };

    loadData() {

        const {processBusinessKey, retryCount} = this.props.params

        apiGateway.report.transactions(processBusinessKey, retryCount).subscribe((activities: any) => {
            this.setState({activities})
            apiGateway.process.businessKey(processBusinessKey, retryCount).subscribe((process: any) => {
                let data: any;
                if (process && process.id) {
                    data = process;
                    this.setState({data, missingInstance: false})
                } else {
                    data = this.recoverProcessInstanceFromActivities(processBusinessKey, activities)
                    this.setState({data, missingInstance: true})
                }
            })
        })

    }

    recoverProcessInstanceFromActivities(processBusinessKey: string, activities: Array<any>) {

        if (activities.length === 0) {
            return {definitionKey: processBusinessKey}
        }

        const firstActivity = activities[0];
        const lastActivity = activities[activities.length - 1];

        let activity = activities.find((it: any) => it.processStatus === 'ERROR')

        if (!activity) {
            activity = activities.find((it: any) => it.processStatus === 'WARNING')
        }
        if (!activity) {
            activity = activities.find((it: any) => it.processStatus === 'PROCESS_COMPLETED')
        }
        if (!activity) {
            activity = lastActivity
        }

        let duration = 0;
        activities.forEach((it: any) => duration += it.duration);

        return {
            processBusinessKey,
            duration,
            definitionKey: firstActivity.definitionKey,
            instanceName: firstActivity.definitionKey,
            instanceStartTime: firstActivity.reportStartTime,
            instanceEndTime: lastActivity.reportEndTime,
            instanceStatus: activity.processStatus,
            instanceDescription: activity.displayName
        }
    }

    showProcesses() {
        this.props.navigate('/process/ALL/' + this.state.data.definitionKey)
    }
}

export default withRouter(ProcessDetails)
