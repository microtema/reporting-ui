import React, {Component} from 'react';
import ProcessState from "../../bootstrap/status/ProcessState";
import DateTime from "../../bootstrap/DateTime";
import Duration from "../../bootstrap/Duration";
import Attachment from "../../bootstrap/Attachment";
import './Activities.scss'
import TransactionId from "../../bootstrap/transaction-id/TransactionId";
import withRouter from "../../../mixin/withRouter";

class Activities extends Component<any, any> {

    render() {
        return <div className="process-activities">
            <table className="table table-striped table-hover">
                <thead>
                <tr>
                    <th scope="col">Name</th>
                    <th scope="col">{this.props.extended ? 'Process Name' : ''}</th>
                    <th scope="col">{this.props.extended ? 'Transaction ID' : ''}</th>
                    <th scope="col" className="retry-count">R</th>
                    <th scope="col" className="date-time">Started</th>
                    <th scope="col" className="time">Ended</th>
                    <th scope="col" className="duration">Duration</th>
                    <th scope="col" className="status">Input</th>
                    <th scope="col" className="status">Output</th>
                    <th scope="col" className="status">Status</th>
                </tr>
                </thead>
                <tbody>
                {
                    this.props.entries.map((it: any, index: number) => {
                        return <tr key={index} onDoubleClick={() => this.showReports(it.id)}>
                            <td>{it.displayName}</td>
                            <td>{this.props.extended ? this.getProcessName(it) : ''}</td>
                            <td>{this.props.extended ? <TransactionId data={it.processBusinessKey}/> : ''}</td>
                            <td>{it.multipleInstanceIndex || ''}</td>
                            <td><DateTime data={it.reportStartTime}/></td>
                            <td><DateTime data={it.reportEndTime} format={'hh:mm:ss'}/></td>
                            <td><Duration data={it.duration}/></td>
                            <td align={'center'}><Attachment data={it.inputPayload}/></td>
                            <td align={'center'}><Attachment data={it.outputPayload} message={it.errorMessage}/></td>
                            <td align={'center'}><ProcessState data={it.processStatus}/></td>
                        </tr>
                    })
                }
                </tbody>
            </table>
        </div>
    }

    showReports(id: string) {

        this.props.navigate('/report/details/' + id)
    }

    getProcessName(entry: any) {

        return entry.processName || entry.definitionKey
    }
}

export default withRouter(Activities)
