import React from 'react';
import * as apiGateway from "../../../api/ApiGateway";
import Duration from "../../bootstrap/Duration";
import DateTime from "../../bootstrap/DateTime";
import ProcessState from "../../bootstrap/status/ProcessState";
import TransactionId from "../../bootstrap/transaction-id/TransactionId";

import './ProcessList.scss'
import Pagination from "../../bootstrap/Pagination";
import {ReloadDataMixin} from "../../../mixin/ReloadDataMixin";
import SearchFilter from "../../bootstrap/SearchFilter";
import withRouter from "../../../mixin/withRouter";

class ProcessList extends ReloadDataMixin {

    constructor(props: any) {
        super(props)
        this.state = {
            entries: [],
            sortBy: 'instanceStartTime',
            totalPages: 0,
            numberOfElements: 25,
            currentElements: 0,
            totalElements: 0,
            page: 0,
            search: '',
            availableOptionList: [{value: "download-as-csv", displayName: "Download as CSV File"}],
            availableDefinitionList: [],
            availableStatusList: [],
            availableTimeRangeList: [],
            selectedDefinition: '',
            selectedStatus: 'ALL',
            selectedTimeRange: 'ALL',
            ...this.props.params
        }
    }

    componentDidMount() {

        super.componentDidMount()

        apiGateway.report.status()
            .subscribe((availableStatusList: any) => this.setState({availableStatusList}))

        apiGateway.timeRange.list()
            .subscribe((availableTimeRangeList: any) => this.setState({availableTimeRangeList}))

        apiGateway.definition.list()
            .subscribe((it: any) => this.setState({availableDefinitionList: it.content}, () => this.loadData()))
    }

    render() {
        return <div className="process-list">
            <br/>
            <div className="row mb-3">
                <div className="col-8">
                    <SearchFilter data={this.state}
                                  onChange={(state: any) => this.loadDataDebounce(state)}
                                  onContextMenuChange={(event: any) => this.onContextMenuChange(event)}/>
                </div>
                <div className="col-4">
                    <Pagination updatePage={(e: any) => this.updatePage(e)} state={this.state}/>
                </div>
            </div>
            <table className="table table-hover table-striped">
                <thead>
                <tr>
                    <th scope="col" className="display-name" onClick={() => this.sortData('instanceName')}>
                        Name <i className={this.getSortClassName('instanceName')}></i>
                    </th>
                    <th scope="col" onClick={() => this.sortData('instanceDescription')}>
                        Description <i className={this.getSortClassName('instanceDescription')}></i>
                    </th>
                    <th scope="col" className="reference" onClick={() => this.sortData('referenceId')}>
                        Reference <i className={this.getSortClassName('referenceId')}></i>
                    </th>
                    <th scope="col" className="transaction" onClick={() => this.sortData('instanceId')}>
                        Transaction <i className={this.getSortClassName('instanceId')}></i>
                    </th>
                    <th scope="col" className="retry-count" onClick={() => this.sortData('retryCount')}>
                        R <i className={this.getSortClassName('retryCount')}></i>
                    </th>
                    <th scope="col" className="date-time" onClick={() => this.sortData('instanceStartTime')}>
                        Time <i className={this.getSortClassName('instanceStartTime')}></i>
                    </th>
                    <th scope="col" className="duration">
                        Duration
                    </th>
                    <th scope="col" className="status" onClick={() => this.sortData('instanceStatus')}>
                        Status <i className={this.getSortClassName('instanceStatus')}></i>
                    </th>
                </tr>
                </thead>
                <tbody>
                {
                    this.state.entries.map((it: any, index: number) => {
                        return <tr key={index} onDoubleClick={this.showReports.bind(this, it)}>
                            <td>{it.instanceName}</td>
                            <td>{it.instanceDescription}</td>
                            <td>
                                <div>{it.referenceType}</div>
                                <div className={'text-muted'}>{it.referenceId}</div>
                            </td>
                            <td><TransactionId data={it.processBusinessKey}/></td>
                            <td>{it.retryCount > 0 ? it.retryCount : '-'}</td>
                            <td>
                                <table className="w-100">
                                    <tbody>
                                    <tr>
                                        <td>
                                            <DateTime data={it.instanceStartTime} format={'DD/MM/yyyy'}/>
                                        </td>
                                        <td align="right">
                                            <DateTime data={it.instanceStartTime} format={'HH:mm:ss'}/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td align="right" className={'text-muted'}>
                                            <DateTime data={it.instanceEndTime} format={'HH:mm:ss'}/>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td><Duration data={it.duration}/></td>
                            <td align={'center'}><ProcessState data={it.instanceStatus}
                                                               title={it.errorMessage || it.instanceDescription}/></td>
                        </tr>
                    })
                }
                </tbody>
            </table>
        </div>
    };

    showReports(entry: any) {

        const {processBusinessKey, retryCount} = entry;

        this.props.navigate('/process/details/' + processBusinessKey + "/" + retryCount)
    }

    updatePage(page: number) {

        const minPage = 0
        const maxPage = this.state.totalPages

        if (page < minPage || page >= maxPage || page === this.state.page) {
            return
        }

        this.loadDataDebounce({page});
    }

    loadData() {

        apiGateway.process.list(this.state)
            .subscribe((it: any) => this.setState({
                entries: it.content,
                totalPages: it.totalPages,
                totalElements: it.totalElements,
                numberOfElements: it.numberOfElements,
                currentElements: it.content.length
            }))
    }

    onContextMenuChange(event: any) {

        if (event === 'download-as-csv') {
            apiGateway.process.download(this.state, 'text/csv');
        }
    }
}

export default withRouter(ProcessList)
