import React from 'react';
import {prettyPrintJson} from 'pretty-print-json'
import * as apiGateway from "../../../api/ApiGateway";
import ProcessState from "../../bootstrap/status/ProcessState";
import DateTime from "../../bootstrap/DateTime";
import Duration from "../../bootstrap/Duration";
import './ReportDetails.scss'
import JSONPretty from "react-json-pretty";
import {ReloadDataMixin} from "../../../mixin/ReloadDataMixin";
import withRouter from "../../../mixin/withRouter";

class ReportDetails extends ReloadDataMixin {

    constructor(props: any) {
        super(props)
        this.state = {
            data: {},
            activities: [],
            formatOutputPayload: false,
            formatInputPayload: false,
            processInstance: {},
            prevActivity: undefined,
            nextActivity: undefined
        }
    }

    render() {
        return <div className="report">
            <br/>
            <div className="row">
                <div className="col-4">
                    <div className="card-subtitle mb-2 text-muted">Name:</div>
                    <div>{this.state.data.displayName}</div>
                </div>
                <div className="col-4">
                    <div className="card-subtitle mb-2 text-muted">Start Time:</div>
                    <div><DateTime data={this.state.data.reportStartTime}/></div>
                </div>
                <div className="col-4">
                    <div className="card-subtitle mb-2 text-muted">Process:</div>
                    <div onClick={() => this.showProcessByBusinessKey()} className="text-decoration-none btn-link">
                        {this.state.processInstance.instanceName || this.state.data.definitionKey}
                    </div>
                </div>
            </div>
            <br/>
            <div className="row">
                <div className="col-4">
                    <div className="card-subtitle mb-2 text-muted">Transaction ID:</div>
                    <div className="text-decoration-none btn-link"
                         onClick={() => this.showProcessByBusinessKey()}>{this.state.data.processBusinessKey}{this.state.processInstance.retryCount > 0 ? (' / ' + this.state.processInstance.retryCount) : ''}</div>
                </div>
                <div className="col-4">
                    <div className="card-subtitle mb-2 text-muted">End Time:</div>
                    <div><DateTime data={this.state.data.reportEndTime}/></div>
                </div>
                <div className="col-4">
                    <div className="card-subtitle mb-2 text-muted">Task Type:</div>
                    <div>{this.getActivityType(this.state.data.activityId)}</div>
                </div>
            </div>
            <br/>
            <div className="row">
                <div className="col-4">
                    <div className="card-subtitle mb-2 text-muted">Status:</div>
                    <div><ProcessState data={this.state.data.processStatus}/></div>
                </div>
                <div className="col-4">
                    <div className="card-subtitle mb-2 text-muted">Duration:</div>
                    <div><Duration data={this.state.data.duration}/></div>
                </div>
                <div className="col-4">
                    <div className="card-subtitle mb-2 text-muted">Reference ID:</div>
                    <div>{this.state.processInstance?.referenceId}</div>
                </div>
            </div>
            <hr/>
            <div className={'row g-2'}>
                <div className="col-md" onClick={() => this.showPrevActivity()}>
                    <div className="btn-link text-decoration-none">
                        <i className={this.state.prevActivity ? 'bi bi-arrow-left-circle' : ''}></i>
                        &nbsp;{this.state.prevActivity?.displayName}
                    </div>
                </div>
                <div className="col-md">
                    <div className="float-end" onClick={() => this.showNextActivity()}>
                        <div className="btn-link text-decoration-none">
                            {this.state.nextActivity?.displayName}&nbsp;
                            <i className={this.state.nextActivity ? 'bi bi-arrow-right-circle' : ''}></i>
                        </div>
                    </div>
                </div>
            </div>
            <hr/>
            <br/>
            <div className="row g-2">
                <div className="col-md">
                    <h5>Input Data</h5>
                </div>
                <div className="col-md">
                    <h6 className="float-end text-decoration-none btn-link"
                        onClick={() => navigator.clipboard.writeText(this.state.data.inputPayload)}>&nbsp;Copy</h6>
                    <h6 className="float-end text-decoration-none btn-link"
                        onClick={() => this.setState({formatInputPayload: !this.state.formatInputPayload})}>Format&nbsp;|</h6>
                </div>
            </div>

            <div className="shadow p-3 mb-5 bg-body rounded">
                {(this.state.formatInputPayload ? <JSONPretty data={this.state.data.inputPayload}></JSONPretty> :
                    <code dangerouslySetInnerHTML={{__html: this.formatJson(this.state.data.inputPayload)}}/>)}
            </div>
            <div className="row g-2">
                <div className="col-md">
                    <h5>Output Data</h5>
                </div>
                <div className="col-md">
                    <h6 className="float-end text-decoration-none btn-link"
                        onClick={() => navigator.clipboard.writeText(this.state.data.outputPayload || this.state.data.errorMessage)}>&nbsp;Copy</h6>
                    <h6 className="float-end text-decoration-none btn-link"
                        onClick={() => this.setState({formatOutputPayload: !this.state.formatOutputPayload})}>Format&nbsp;|</h6>
                </div>
            </div>
            <div className="shadow p-3 mb-5 bg-body rounded">
                {(this.state.formatOutputPayload ? <JSONPretty data={this.state.data.outputPayload}></JSONPretty> :
                    <code
                        dangerouslySetInnerHTML={{__html: this.formatJson(this.state.data.outputPayload || this.state.data.errorMessage)}}/>)}
            </div>
        </div>
    };

    showPrevActivity() {

        if (!this.state.prevActivity) {
            return;
        }

        this.showReports(this.state.prevActivity.id)
    }

    showNextActivity() {

        if (!this.state.nextActivity) {
            return;
        }

        this.showReports(this.state.nextActivity.id)
    }

    getActivityType(activityId: string) {

        return (activityId || '').split('_')[0]
    }

    formatJson(json: string) {

        try {
            const data = JSON.parse(json)

            return prettyPrintJson.toHtml(data, {quoteKeys: true})
        } catch (e) {
            return json;
        }
    }

    showProcessByBusinessKey() {

        this.props.navigate('/process/details/' + this.state.data.processBusinessKey + "/" + this.state.data.retryCount);
    }

    loadProcess() {

        const {processBusinessKey, retryCount} = this.state.data

        apiGateway.process.businessKey(processBusinessKey, retryCount)
            .subscribe((processInstance: any) => this.setState({processInstance}))

        apiGateway.report.transactions(processBusinessKey, retryCount)
            .subscribe((activities: any) => {

                let prevActivity = false;
                let nextActivity = false;

                for (let index = 0; index < activities.length; index++) {

                    const activity = activities[index];

                    if (activity.id === this.state.data.id) {

                        if (index > 0) {
                            prevActivity = activities[index - 1]
                        }

                        if (index < activities.length) {
                            nextActivity = activities[index + 1]
                        }
                    }
                }

                this.setState({prevActivity, nextActivity, activities});
            })
    }

    loadData(data: any) {

        let id = this.props.params.id as string
        if (data && data.id) {
            id = data.id;
        }

        apiGateway.report.get(id).subscribe((it: any) => this.setState({data: it}, () => this.loadProcess()))
    }

    showReports(id: string) {

        this.componentWillUnmount();

        this.loadData({id})

        this.props.navigate('/report/details/' + id)
    }
}

export default withRouter(ReportDetails)
