import React from 'react';
import * as apiGateway from "../../../api/ApiGateway";
import Activities from "../../process/details/Activities";
import Pagination from "../../bootstrap/Pagination";
import SearchFilter from "../../bootstrap/SearchFilter";
import {ReloadDataMixin} from "../../../mixin/ReloadDataMixin";
import withRouter from "../../../mixin/withRouter";

class ReportList extends ReloadDataMixin {

    constructor(props: any) {
        super(props)
        this.state = {
            entries: [],
            totalPages: 0,
            numberOfElements: 25,
            currentElements: 0,
            totalElements: 0,
            page: 0,
            availableOptionList: [{value: "download-as-csv", displayName: "Download as CSV File"}],
            availableDefinitionList: [],
            availableStatusList: [],
            availableTimeRangeList: [],
            selectedDefinition: '',
            selectedStatus: 'ALL',
            selectedTimeRange: 'ALL',
            search: '',
        }
    }

    componentDidMount() {

        super.componentDidMount()

        apiGateway.report.status()
            .subscribe((availableStatusList: any) => this.setState({availableStatusList}))

        apiGateway.timeRange.list()
            .subscribe((availableTimeRangeList: any) => this.setState({availableTimeRangeList}))

        apiGateway.definition.list()
            .subscribe((it: any) => this.loadDataDebounce({availableDefinitionList: it.content}))
    }

    render() {

        return <div className="report-list">
            <br/>
            <div className="row mb-3">
                <div className="col-8">
                    <SearchFilter data={this.state}
                                  onChange={(state: any) => this.loadDataDebounce(state)}
                                  onContextMenuChange={(event: any) => this.onContextMenuChange(event)}/>
                </div>
                <div className="col-4">
                    <Pagination updatePage={(e: any) => this.updatePage(e)} state={this.state}/>
                </div>
            </div>
            <Activities definitions={this.state.definitions} extended={true} entries={this.state.entries || []}/>
        </div>
    };

    updatePage(page: number) {

        const minPage = 0
        const maxPage = this.state.totalPages

        if (page < minPage || page >= maxPage || page === this.state.page) {
            return
        }

        this.loadDataDebounce({page})
    }

    loadData() {

        apiGateway.report.list(this.state)
            .subscribe((it: any) => this.setState({
                entries: it.content,
                totalPages: it.totalPages,
                totalElements: it.totalElements,
                numberOfElements: it.numberOfElements,
                currentElements: it.content.length
            }))
    }

    onContextMenuChange(event: any) {

        if (event === 'download-as-csv') {
            apiGateway.report.download(this.state, 'text/csv');
        }
    }
}

export default withRouter(ReportList)
