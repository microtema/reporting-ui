import React from 'react';
import * as apiGateway from "../../../api/ApiGateway";
import ProcessState from "../../bootstrap/status/ProcessState";

import './ProcessMessageList.scss'
import Pagination from "../../bootstrap/Pagination";
import {ReloadDataMixin} from "../../../mixin/ReloadDataMixin";
import SearchFilter from "../../bootstrap/SearchFilter";
import withRouter from "../../../mixin/withRouter";

class ProcessMessageList extends ReloadDataMixin {

    constructor(props: any) {
        super(props)
        this.state = {
            entries: [],
            sortBy: 'totalCount',
            totalPages: 0,
            numberOfElements: 25,
            currentElements: 0,
            totalElements: 0,
            page: 0,
            search: '',
            availableOptionList: [{value: "download-as-csv", displayName: "Download as CSV File"}],
            availableDefinitionList: [],
            availableStatusList: [],
            availableTimeRangeList: [],
            selectedDefinition: '',
            selectedStatus: 'ALL',
            selectedTimeRange: 'ALL',
            ...this.props.params
        }
    }

    componentDidMount() {

        super.componentDidMount()

        apiGateway.report.status()
            .subscribe((availableStatusList: any) => this.setState({availableStatusList}))

        apiGateway.timeRange.list()
            .subscribe((availableTimeRangeList: any) => this.setState({availableTimeRangeList}))

        apiGateway.definition.list()
            .subscribe((it: any) => this.setState({availableDefinitionList: it.content}, () => this.loadData()))
    }

    render() {
        return <div className="process-message-list">
            <br/>
            <div className="row mb-3">
                <div className="col-8">
                    <SearchFilter data={this.state}
                                  onChange={(state: any) => this.loadDataDebounce(state)}
                                  onContextMenuChange={(event: any) => this.onContextMenuChange(event)}/>
                </div>
                <div className="col-4">
                    <Pagination updatePage={(e: any) => this.updatePage(e)} state={this.state}/>
                </div>
            </div>
            <table className="table table-hover table-striped">
                <thead>
                <tr>
                    <th scope="col" onClick={() => this.sortData('errorMessage')}>
                        Message <i className={this.getSortClassName('errorMessage')}></i>
                    </th>
                    <th scope="col" className="duration" onClick={() => this.sortData('totalCount')}>
                        Count <i className={this.getSortClassName('totalCount')}></i>
                    </th>
                    <th scope="col" className="status">
                        Status
                    </th>
                </tr>
                </thead>
                <tbody>
                {
                    this.state.entries.map((it: any, index: number) => {
                        return <tr key={index}>
                            <td className={'text-truncate'} title={it.errorMessage}>{it.errorMessage}</td>
                            <td>{it.totalCount}</td>
                            <td align={'center'}><ProcessState data={this.state.selectedStatus}/></td>
                        </tr>
                    })
                }
                </tbody>
            </table>
        </div>
    };

    updatePage(page: number) {

        const minPage = 0
        const maxPage = this.state.totalPages

        if (page < minPage || page >= maxPage || page === this.state.page) {
            return
        }

        this.loadDataDebounce({page});
    }

    loadData() {

        apiGateway.message.list(this.state)
            .subscribe((it: any) => this.setState({
                entries: it.content,
                totalPages: it.totalPages,
                totalElements: it.totalElements,
                numberOfElements: it.numberOfElements,
                currentElements: it.content.length
            }))
    }

    onContextMenuChange(event: any) {

        if (event === 'download-as-csv') {
            apiGateway.message.download(this.state, 'text/csv');
        }
    }
}

export default withRouter(ProcessMessageList)
