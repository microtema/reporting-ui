import express from 'express'
import * as path from 'path'
import {createProxyMiddleware} from 'http-proxy-middleware';
import gitInfo from './git-info.json'

const webServer = express()

const PORT = process.env.PORT || 3030

const services = (process.env.REST_API_SERVERS || '').split(',').filter(it => it)

services.forEach(it => {

    const url = new URL(it + '/rest/api')
    const service = url.pathname.split('/')[1]

    const filter = '/api/' + service
    const target = url.protocol + '//' + url.host
    const pathRewrite = {['^' + filter]: url.pathname}

    webServer.use(filter, createProxyMiddleware({changeOrigin: true, target, pathRewrite}))
})

// Expose build folder as static
webServer.use(express.static(path.join(__dirname, '..', '..', 'build')))

// Endpoint for readyness check
webServer.get('/reporting-ui/git/info', (req: any, res: any) => res.json(gitInfo));

// Serving Apps with Client-Side Routing
webServer.get('/*', (req: any, res: any) => res.sendFile(path.join(__dirname, '..', '..', 'build', 'index.html')));

// Listen for requests
webServer.listen(PORT, () => {

    console.log(`Server is running on http://localhost:${PORT}.`)
    console.log(`Server is serving on ${path.join(__dirname, '..', '..', 'build', 'index.html')}.`)

    services.forEach(it => console.log(`Server is consuming from REST API ${it}`))
})
