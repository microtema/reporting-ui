"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const path = __importStar(require("path"));
const http_proxy_middleware_1 = require("http-proxy-middleware");
const git_info_json_1 = __importDefault(require("./git-info.json"));
const webServer = express_1.default();
const PORT = process.env.PORT || 3030;
const services = (process.env.REST_API_SERVERS || '').split(',').filter(it => it);
services.forEach(it => {
    const url = new URL(it + '/rest/api');
    const service = url.pathname.split('/')[1];
    const filter = '/api/' + service;
    const target = url.protocol + '//' + url.host;
    const pathRewrite = { ['^' + filter]: url.pathname };
    webServer.use(filter, http_proxy_middleware_1.createProxyMiddleware({ changeOrigin: true, target, pathRewrite }));
});
// Expose build folder as static
webServer.use(express_1.default.static(path.join(__dirname, '..', '..', 'build')));
// Endpoint for readyness check
webServer.get('/reporting-ui/git/info', (req, res) => res.json(git_info_json_1.default));
// Serving Apps with Client-Side Routing
webServer.get('/*', (req, res) => res.sendFile(path.join(__dirname, '..', '..', 'build', 'index.html')));
// Listen for requests
webServer.listen(PORT, () => {
    console.log(`Server is running on http://localhost:${PORT}.`);
    console.log(`Server is serving on ${path.join(__dirname, '..', '..', 'build', 'index.html')}.`);
    services.forEach(it => console.log(`Server is consuming from REST API ${it}`));
});
//# sourceMappingURL=server.js.map