const path = require("path");
const getRepoInfo = require('git-repo-info');
const writeJsonFile = require('write-json-file');

const info = getRepoInfo();
const gitInfo = {commitId: info.sha}
const jsonPath = `${path.join(__dirname, 'git-info.json')}`

writeJsonFile(jsonPath, gitInfo).then(() => console.log('Write git-info -> ' + jsonPath, gitInfo))
